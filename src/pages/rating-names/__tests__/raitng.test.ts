import {expect, test} from '@playwright/test';

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page   }) => {
    await page.route(
        request => request.href.includes('/api/likes/cats/rating'),
        async route => {
            await route.abort();
        }
    );

    await page.goto(
        '/rating'
    );
    await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается', async ({ page   }) => {

     await page.goto(
              '/rating'
         );
    await page.waitForSelector('table.rating-names_table__Jr5Mf');
    const likes = await page.locator('table.rating-names_table__Jr5Mf:first-child td:last-child').allTextContents();
    const sortedLikes = [...likes].sort((a, b) => parseInt(b) - parseInt(a));
    expect(likes).toEqual(sortedLikes);
});
